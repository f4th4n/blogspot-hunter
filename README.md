###Installation###
- Clone this repo
- Install dependency
```$ npm install```
```$ bundle install ```
```$ node_modules/.bin/selenium-standalone install```

###How to run###

####Run in Child Server####
- Start selenium server
- Start bin/check_availability_blogspot
- Start bin/check_statistics
- Start bin/check_status
- Start bin/hunt_blogspot
- Start bin/yahoo

####Run in Host Server####
- Put folder html in master server
```$ cp -rf html /var/www/html/```
- Start ```bin/master_check_availability```
- Cron hourly ```php bin/pull_from_child.php```

###Dependency###
- Selenium server
- Watir
- Webdriverio

###Todo
ALTER TABLE `blogspot` ADD `mozrank` FLOAT NOT NULL AFTER `page_authority`;