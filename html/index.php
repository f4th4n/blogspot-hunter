<?php
$captcha_submited = !empty($_POST['captcha']);
if($captcha_submited) {
    $captcha_txt = fopen("captcha.txt", "w") or die("Unable to open file!");
    $txt = $_POST['captcha'];
    fwrite($captcha_txt, $txt);
    fclose($captcha_txt);

    $data_encoded = file_get_contents('data.json');
    $data_decoded = json_decode($data_encoded);
    $data_decoded->need_captcha = 'false';
    $new_data = json_encode($data_decoded);
    $captcha_txt = fopen("data.json", "w") or die("Unable to open file!");
    fwrite($captcha_txt, $new_data);
    fclose($captcha_txt);
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Hunter</title>
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon" class="favicon">
    <link rel="icon" href="assets/favicon.ico" type="image/x-icon" class="favicon">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style src="assets/style.css"></style>
</head>
<body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <div id="ready-to-register"></div>
            <br />
            <button id="clear-blogspot-pa">Clear Blogspot</button>
            <hr />
            <hr />
        </div>
      </div>
      <div class="row">
        <!--
        <div class="col-md-4 col-xs-12 col-sm-12">
            <form action="" method="post">
                <input type="text" name="captcha" id="input_captcha">
                <button>Submit</button>
            </form>
            <img src="" id="ss_captcha">
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12">
            <div>
                <h3>Insert Query</h3>
                <form action="insert_query.php" method="post">
                    <textarea name="list_names"></textarea>
                    <br />
                    <button>Insert</button>
                </form>
            </div>
        </div>
        -->
      </div>
    </div>

    <script type="text/javascript">
        function check_captcha() {
            var d = new Date();
            $("#ss_captcha").attr("src", "ss_captcha.png?" + d.getTime());
            $.ajax({
                url: "data.json", 
                    success: function(res_ajax_captcha) {
                    var need_captcha = res_ajax_captcha.need_captcha
                    if(need_captcha == 'true') {
                        $(".favicon").attr("href","assets/red-favicon.ico?" + d.getTime());
                    } else {
                        $(".favicon").attr("href","assets/green-favicon.ico?" + d.getTime());
                    }
                }
            });
        }

        function check_register() {
            $.ajax({url: "ready_to_register.php", success: function(result) {
                $('#ready-to-register').html(result)
            }});
        }

        function clear_blogspot_pa() {
            $.ajax({url: "clear_blogspot_pa.php", success: function(result) {
                window.location = self.location;;
            }});
        }

        // on load
        $(document).ready(function() {
            check_register()
            //check_captcha()    
            $("#input_captcha").focus()
            $('#ss_captcha').click(function() {
                $("#input_captcha").focus()
            })

            // reload every
            //setInterval(check_register, 10000)
            //setInterval(check_captcha, 2000)
            $('#clear-blogspot-pa').click(clear_blogspot_pa)
        })
    </script>
</body>
</html>
