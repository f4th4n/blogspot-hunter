<?php
require('config_database.php');

if(empty($_POST['list_names']))
    die('die');

$list_names = preg_split('/\r\n|[\r\n]/', $_POST['list_names']);

$sql = 'INSERT IGNORE INTO query (query_string) VALUES ';
foreach($list_names as $name) {
    if(empty($name))
        continue;
    $name = preg_replace("/[^a-zA-Z0-9\s]+/", "", $name);
    $name = mysqli_real_escape_string($conn, $name);
    $sql .= '(\'';
    $sql .= $name;
    $sql .= '\'),';
}

// remove last char
$sql = rtrim($sql, ",");

$conn->query($sql);
header('Location: index.php');
