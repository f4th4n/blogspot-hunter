<?php
require('config_database.php');

$count_unvisited_blogspot = $conn->query("select * from web2 where status='unknown'");
$count_unvisited_blogger_profile = $conn->query("select * from blogger_profile where is_visited=0");
$count_unvisited_query = $conn->query("select * from query where is_visited=0");
$list_blogspot_pa_ready_to_register = $conn->query("select * from web2 where page_authority  > 1 and status='ready_to_register' order by page_authority desc");
$list_blogspot_alexa_ready_to_register = $conn->query("select * from web2 where alexa_rank  > 0 and status='ready_to_register'");
?>

<?php function print_table($list_blogspot, $title) { ?>
    <br />
    <h2><?= $list_blogspot->num_rows ?> <?= $title ?></h2> 
    <br />
    <?php if ($list_blogspot->num_rows > 0) : ?>
        <table class="table table-striped">
            <tr>
                <td>URL</td>
                <td>PA</td>
                <td>Mozrank</td>
                <td>Alexa</td>
                <td>Action</td>
            </tr>    
        <?php while($row = $list_blogspot->fetch_assoc()) : ?>
            <tr>
                <td><a href="http://<?= $row["url"] ?>.blogspot.com" target="_blank"><?= $row["url"] ?></a></td>
                <td><?= $row["page_authority"] ?></td>
                <td><?= $row["mozrank"] ?></td>
                <td><?= number_format( $row["alexa_rank"] ) ?></td>
    	        <td>
                    <a href="delete.php?id=<?= $row["id"] ?>"> Delete </a>
                </td>
            </tr>
        <?php endwhile ?>
        </table>
    <?php else : ?>
        0 Results
    <?php endif ?>
<?php } ?>

<?= print_table($list_blogspot_alexa_ready_to_register, 'Blogspot Alexa Rank') ?>
<?= print_table($list_blogspot_pa_ready_to_register, 'Blogspot Page Authority') ?>

<?php
$conn->close();
