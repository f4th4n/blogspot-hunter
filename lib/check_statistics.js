var Mozscape    = require('mozscape').Mozscape;
var parseString = require('xml2js').parseString;
var request = require('request');
var config      = require('../config.json');
var model       = require('./model')
const chalk = require('chalk');

function get_next_moz_account() {
    if(this.counter === undefined)
        this.counter = 0

    var moz_account = config.moz_account
    var next_val = moz_account[this.counter]
    this.counter++
    if(this.counter == moz_account.length)
        this.counter = 0
    return next_val
}

function check_page_authority(rows_blogspot) {
    var moz_account = get_next_moz_account()
    var moz = new Mozscape(moz_account.user, moz_account.key);
    if(rows_blogspot === null || rows_blogspot.length === 0)
        process.exit()
    var row_blogspot = rows_blogspot.pop()
    var url = row_blogspot.url + '.blogspot.com'
    moz.urlMetrics(url, ['page_authority', 'mozRank'], function(err, res) {
        console.log(res + '\t' + moz_account.user + '\n' + url)
        var that_rows_blogspot = rows_blogspot
        var that_row_blogspot = row_blogspot
        if (err || res.upa == undefined) {
            that_rows_blogspot.push(that_row_blogspot)
            check_page_authority(that_rows_blogspot)
        } else {
            console.log('PA ' + chalk.white(res.upa) + '\t' + 'Mozrank ' + chalk.white(res.umrp) + '\t' + url)
            model.update_row_moz(row_blogspot.id, res.upa, res.umrp)
            check_page_authority(that_rows_blogspot)
        }
    });
}

function check_alexa_rank(rows_blogspot) {
    if(rows_blogspot === null || rows_blogspot.length === 0)
        return

    var row_blogspot = rows_blogspot.pop()
    var url = 'http://data.alexa.com/data?cli=10&url=' + row_blogspot.url + '.blogspot.com'
    request(url, function (error, response, body) {
        var that_rows_blogspot = rows_blogspot
        var that_row_blogspot = row_blogspot
        var that_url = url
        if (!error && response.statusCode == 200) {
            var xml = body
            parseString(xml, function (err, result) {
                try{
                    var rank = result['ALEXA']['SD'][0]['REACH'][0]['$']['RANK']
                    model.update_row_alexa_rank(that_row_blogspot.id, rank)
                    //console.log(' Alexa Rank : ' + rank + ' ' + that_rows_blogspot.url)
                    check_alexa_rank(that_rows_blogspot)
                } catch ( e ) {
                    model.update_row_alexa_rank(that_row_blogspot.id, '0 ' + that_rows_blogspot.url)
                    //console.log(' Alexa Rank : 0')
                    check_alexa_rank(that_rows_blogspot)
                }
            });
        } else {
            console.log('error request, skip')
        }
    })
}

model.get_where_ready_to_register_and_no_pa(function(err, rows_blogspot) {
    if (err) {
        throw err;
    }
    check_page_authority(rows_blogspot)
})

model.get_where_dead_and_no_alexa(function(err, rows_blogspot) {
    if (err) {
        throw err;
    }
    check_alexa_rank(rows_blogspot)
})
