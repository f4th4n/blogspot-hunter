var config     = require('../config.json');

exports.get_user_agent = function get_user_agent() {
    var user_agent = config.user_agent
    var rand_val = user_agent[Math.floor(Math.random() * user_agent.length)]
    return rand_val
}

exports.datetime = function datetime() {
	var d = new Date();
	var datetime = d.toLocaleTimeString();
    return datetime
}