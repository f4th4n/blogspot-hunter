var path = require('path')

var hunter = {}

hunter.start = function(hunterType) {
	if(hunterType != 'slave' && hunterType != 'master') {
		throw "Unknown type"
	}

	if(hunterType == 'master') {
		//this.startServer()
		this.startStatistic()
	}
}

hunter.startServer = function() {
	var express = require('express')
	var app = express()

	var htmlPath = path.join(__dirname, '../html')
	console.log(htmlPath)
	app.use('/', express.static(htmlPath))

	app.listen(3001, function () {
		console.log('Server start')
	})
}

hunter.startStatistic = function() {
	var statistic = require('./check_statistics')
	statistic.on('stop', function(err, cb) {
		setTimeout(function() {
			console.log('restart')
		}, 2000)
	})
}

module.exports = hunter