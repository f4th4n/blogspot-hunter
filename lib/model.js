var config     = require('../config.json');
var mysql      = require('mysql');

var connection = mysql.createConnection({
    host     : config.db_host,
    user     : config.db_user,
    password : config.db_password,
    database : config.db_database
});

exports = module.exports = function() {}

connection.connect()

exports.update_row_moz = function(id, page_authority, mozrank) {
    connection.query('update web2 set page_authority = ?, mozrank = ? where id = ?', [page_authority, mozrank, id])
}

exports.update_row_alexa_rank = function(id, alexa_rank) {
    connection.query('update web2 set alexa_rank = ? where id = ?', [alexa_rank, id])
}

exports.update_row_status = function(id, status) {
    connection.query('update web2 set status = ?, last_update=CURRENT_TIMESTAMP where id = ?', [status, id])
}

exports.get_where_dead_and_no_pa = function(next) {
    connection.query('select id, url from web2 where (status=\'dead\' or status=\'ready_to_register\') and page_authority=0 order by id desc', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_where_dead = function(next) {
    connection.query('select id, url from web2 where status=\'dead\' order by id desc', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_where_deleted = function(next) {
    connection.query('select id, url from web2 where status=\'deleted\' order by id desc', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_where_ready_to_register_and_no_pa = function(next) {
    connection.query('select id, url from web2 where status=\'ready_to_register\' and page_authority=0 order by id desc', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_where_dead_and_no_alexa = function(next) {
    var sql = 'select id, url from web2 where (status=\'dead\' or status=\'ready_to_register\') and alexa_rank is null order by RAND() limit 100'
    connection.query(sql, function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_where_dead_with_pa = function(next) {
    connection.query('select * from web2 where status=\'dead\' and page_authority>1', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_where_unknown = function(next) {
    connection.query('select * from web2 where status=\'unknown\'', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_random_where_unknown = function(next) {
    connection.query('select * from web2 where status=\'unknown\' order by RAND() LIMIT 1', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.get_sample = function(next) {
    connection.query('select id, url from web2 limit 20', function(err, rows_blogspot, fields) {
        next(err, rows_blogspot)
    });
}

exports.delete_by_id = function(id) {
    connection.query('delete from web2 where id = ?', [id]);
}

exports.get_query = function(next) {
    connection.query('select * from query', function(err, rows_query, fields) {
        next(err, rows_query)   
    });
}

exports.get_random_query = function(next) {
    connection.query('select * from query order by RAND() LIMIT 1', function(err, rows_query, fields) {
        next(err, rows_query)   
    });
}

exports.get_random_unvisited_query = function(next) {
    connection.query('select * from query where is_visited=0 order by RAND() LIMIT 1', function(err, rows_query, fields) {
        next(err, rows_query)   
    });
}

exports.visit_query = function(query_string) {
    connection.query('update query set is_visited = 1 where query_string = ?', [query_string]);
}

exports.insert_blogger_profile = function(blogger_profile) {
    connection.query('insert ignore blogger_profile (profile_id) values ?', [blogger_profile])
}
