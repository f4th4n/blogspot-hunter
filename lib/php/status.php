<?php
$config = json_decode(file_get_contents(CONFIG_PATH));

function f_exec() {
	global $config;
	$username = $config->db_user;
	$password = $config->db_password;
	$local_conn = new mysqli('localhost', $username, $password, $config->db_database);

	$sql = "SELECT * FROM web2 where status='unknown'";
	$result = $local_conn->query($sql);
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	    	$id = $row['id'];
	    	$url = 'http://' . $row['url'] . '.blogspot.com';
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT,10);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$output = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			$contain_register_button = strpos($output, 'maia-button maia-button-primary') !== false;
			$is_404 = ($httpcode == 404);
			if($contain_register_button && $is_404) {
				$sql = "update blogspot set status='ready_to_register' where id='$id'";
				echo '+ ' . $row['url'] . PHP_EOL;
			} else {
				$sql = "delete from blogspot where id='$id'";				
				echo '- ' . $row['url'] . PHP_EOL;
			}
			$local_conn->query($sql);
	    }
	}
}

while(true) {
	f_exec();
	sleep(5);
}