require 'json'

def get_json_config ()
    app_dir = File.expand_path("../..", File.dirname(__FILE__))
    config_address = app_dir + '/config.json'
    data_encoded = File.read( config_address )
    data_decoded = JSON.parse(data_encoded)
    data_decoded
end

$config = get_json_config
DB_USERNAME = $config['db_user']
DB_PASSWORD = $config['db_password']
DB_HOST     = $config['db_host']
DATABASE    = $config['db_database']
PUBLIC_HTML = $config['public_html']
