require 'watir'
require 'watir-webdriver'
require 'watir/extensions/element/screenshot'
require 'open-uri'
require 'mysql2'
require 'faraday'
require "net/https"
require "uri"
require 'mail'
require 'deathbycaptcha'
require_relative "config"

Watir.default_timeout = 10

$death_by_captcha_client = DeathByCaptcha.new($config['death_by_captcha_username'], $config['death_by_captcha_password'], :http)
$public_html = PUBLIC_HTML
$db = Mysql2::Client.new(:host => DB_HOST, :username => DB_USERNAME, :password => DB_PASSWORD, :database => DATABASE)
$b  = Watir::Browser.new :phantomjs
capabilities = Selenium::WebDriver::Remote::Capabilities.phantomjs(
    "phantomjs.page.settings.userAgent" => "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
    "phantomjs.page.settings.loadImages" => false
    )
Selenium::WebDriver.for :phantomjs, :desired_capabilities => capabilities

def need_captcha (value)
    captcha_address = $public_html + '/assets/data.json'
    data_encoded = File.read( captcha_address )
    data_decoded = JSON.parse(data_encoded)
    data_decoded['need_captcha'] = value
    new_data = data_decoded.to_json
    open(captcha_address, 'w') { |f|
        f.puts new_data
    }
end

def send_mail_captcha()
    puts "Send Email not active... "
    # Mail.deliver do
    #        to 'wilfathan@gmail.com'
    #      from 'wilfathan@gmail.com'
    #   subject 'Need Captcha [FFFFFF]'
    #      body "goto http://hunter.fathan.info to solve captcha"
    # end
end

def clear_ss_captcha ()
    app_dir = File.expand_path("../..", File.dirname(__FILE__))
    %x(rm -rf #{$public_html}/assets/ss_captcha.png)
    %x(cp -rf #{$public_html}/assets/ok.png #{$public_html}/assets/ss_captcha.png)
end

def get_captcha_text()
    captcha_address = $public_html + '/assets/captcha.txt'
    captcha_text = File.read( captcha_address )
    return captcha_text
end

def clear_captcha_text()
    captcha_address = $public_html + '/assets/captcha.txt'
    File.truncate(captcha_address, 0)
end

def solve_captcha_manual
    ss_address = $public_html + '/assets/ss_captcha.png'
    $b.img.screenshot ss_address 
    need_captcha 'true'
    send_mail_captcha
    captcha_waiting_counter = 0
    while true
        solve_captcha_message = 'please solve captcha hunter.fathan.info ' + captcha_waiting_counter.to_s
        puts solve_captcha_message
        captcha_waiting_counter = captcha_waiting_counter + 1
        sleep(1)
        if captcha_waiting_counter == 1000
            exit
        end
        if get_captcha_text != ''
            $b.text_field(:id => 'captcha').set get_captcha_text
            clear_ss_captcha
            clear_captcha_text
            $b.form().submit
            # sleep(2)
            return 0
        end
    end
end

def destroy_current_droplet
    puts 'destroy'
end

def solve_captcha_auto
    destroy_current_droplet
    return 0
    ss_address = $public_html + '/assets/ss_captcha.png'
    $b.img.screenshot ss_address
    captcha = $death_by_captcha_client.decode!(path: ss_address)
    $b.text_field(:id => 'captcha').set captcha.text
    clear_ss_captcha
    $b.form().submit
end

clear_ss_captcha
$url_before = ''
$blogger_profile_counter = 1
$db.query( "select * from blogger_profile where is_visited=0" ).each do |row|
    profile_id = row["profile_id"]
    url = "https://www.blogger.com/profile/#{profile_id}"
    puts "check profile #{profile_id}"
    list_blogspot = []

    if($blogger_profile_counter % 30 == 0)
        exit
    end
    $blogger_profile_counter = $blogger_profile_counter + 1

    $b.goto(url) rescue nil

    if($b.input(:id => 'captcha').exist?)
        if $config['solve_captcha'] == 'auto'
            solve_captcha_auto
        else
            solve_captcha_manual
        end
    #end rescue nil
    end

    $b.links.each do |a|
        href = a.href
        contain_blogspot   = href.include? 'blogspot.com'
        contain_bp         = href.include? 'bp.blogspot'
        contain_mailto     = href.include? 'mailto:'
        contain_underscore = href.include? '_'
        if contain_blogspot && !contain_bp && !contain_mailto && !contain_underscore
            list_blogspot.push href
        end
    end rescue nil

    query_append = ''
    list_blogspot = list_blogspot.uniq
    list_blogspot.each do |blogspot|
        blog_name = blogspot.sub('http://', '').split(/\./).first
        query_append = query_append + " ('#{blog_name}', 'unknown', 0),"
        puts " + #{blog_name} [status unknown]"
    end

    query_append.chop! # remove last character (,)
    query = "insert ignore into blogspot (url, status, page_authority) values #{query_append}"
    if list_blogspot.length > 0
        $db.query(query)
        puts "inserting data..."
    end
    
    id_row = row["id"]
    $db.query("update blogger_profile set is_visited=1 where id=#{id_row}")
end

at_exit do
    # Mail.deliver do
    #        to 'wilfathan@gmail.com'
    #      from 'wilfathan@gmail.com'
    #   subject 'Blogspot Hunter Finish [FFFFFF]'
    #      body "Detail "
    # end
    puts "exit"
end
