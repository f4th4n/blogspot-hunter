var config      = require('../config.json');
var model       = require('./model')
var webdriverio = require('webdriverio')
var worker = config.yahoo_worker
var search_result_counter = 0
var search_result_cache = []
var push_every = 10
var debug_mode = false

var events = require('events')
var util = require('util')
var yahoo = new events.EventEmitter()

/*
 *  pseudo-code
 *  get query where is not visited
 *  loop query
 *      goto https://www.yahoo.com/
 *      fill #UHSearchBox val '{query} site:blogger.com/profile -Google+'
 *      click #UHSearchWeb
 *      get all href of .searchCenterMiddle div > div.compTitle.options-toggle > h3 > a
 *      if .next exist
 *          click .next
 *      else
 *          screenshot page
 *          next query
 *  loop end
 */

yahoo.exit_handler = function exit_handler() {
    process.stdin.resume();//so the program will not close instantly

    function exitHandler(options, err) {
        if(debug_mode) {
            console.log('exit triggered')
        }
        if(yahoo.client != null) {
            yahoo.client.end()
        }
        if (options.cleanup) console.log('clean');
        if (err) console.log(err.stack);
        if (options.exit) process.exit();
    }

    //do something when app is closing
    process.on('exit', exitHandler.bind(null,{cleanup:true}));

    //catches ctrl+c event
    process.on('SIGINT', exitHandler.bind(null, {exit:true}));

    //catches uncaught exceptions
    process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
}
yahoo.exit_handler()

yahoo.handle_result = function handle_result(ret) {
    if(debug_mode) {
        console.log('checkpoint all_links_node')
    }

    if(ret.value == []) {
        return;
    }

    var all_links_arr = ret.value
    var blogger_profile_arr = all_links_arr.map(function(v) {
        return v.replace(/[^0-9]/g, '')
    })
    blogger_profile_arr.forEach(function(blogger_profile) {
        search_result_cache.push([blogger_profile])
        search_result_counter++
        console.log(search_result_counter + ' ' + blogger_profile)
        if(search_result_counter % push_every === 0) {
            model.insert_blogger_profile(search_result_cache)
            // kosongkan search_result_cache setelah push
            search_result_cache = []
        }
    })
}

yahoo.check_if_next_page_visible = function check_if_next_page_visible(is_visible) {
    if(debug_mode) {
        console.log('is_visible is ')
        console.log(is_visible)
    }

    if(is_visible) {
        console.log('next page')
        yahoo.client.click('.next')
            .pause(5000)
            .then(yahoo.yahoo_search_exec)
    } else {
        console.log('no next, close tab')
        yahoo.start()
    }
}

yahoo.yahoo_search_exec = function yahoo_search_exec() {
    if(debug_mode) {
        console.log('Checkpoint yahoo_search_exec')
    }

    function get_all_links() {
        var all_links_arr = []
        var all_links_node = document.querySelectorAll('.searchCenterMiddle div > div.compTitle.options-toggle > h3 > a')
        for(var i = 0; i < all_links_node.length; i++) {
            var href = all_links_node[i].href
            all_links_arr.push(href)
        }
        return all_links_arr
    }

    yahoo.client.pause(4000)
        .execute(get_all_links)
        .then(yahoo.handle_result)
        .isVisible('.next')
        .then(yahoo.check_if_next_page_visible)
}

yahoo.start_browser = function start_browser(query_string) {
    var url = 'https://id.yahoo.com/'
    var options = {
        desiredCapabilities: {
            browserName: 'phantomjs'
        }
    };

    var viewport_size = {
        width : 1366,
        height : 768
    }
   
    var query = query_string + ' site:blogger.com/profile -Google+'
    yahoo.client = webdriverio.remote(options);
    yahoo.client
        .init()
        .setViewportSize(viewport_size)
        .windowHandleMaximize()
        .url(url)
        .setValue('#UHSearchBox', query)
        .click('#UHSearchWeb')
        .then(yahoo.yahoo_search_exec)
}

yahoo.start = function start() {
    // if there is browser running, kill them first
    if(yahoo.client != undefined) {
        yahoo.client.end()
    }

    model.get_random_unvisited_query(function(err, rows_query) {

        if (err) {
            throw err;
        }
        
        if(rows_query == false) {
            process.exit()
        }

        var query_string = rows_query[0].query_string
        query_string = query_string.replace(/[^a-z]/gi, '')
        if(debug_mode) {
            console.log('query_string')
            console.log(query_string)
        }
        model.visit_query(query_string)
        yahoo.start_browser(query_string)
    })
}

for(var worker_counter = 0; worker_counter < worker; worker_counter++) {
    yahoo.start()
}
